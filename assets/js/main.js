$(".shop-menu ul li").click(function(){
	$(".shop-menu ul li").removeClass("active");
	$(this).addClass("active");

	let selector = $(this).attr("data-filter");
	$(".shop-items").isotope({
		filter: selector
	});

});